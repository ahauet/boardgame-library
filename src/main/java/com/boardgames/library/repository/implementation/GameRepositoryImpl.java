package com.boardgames.library.repository.implementation;

import com.boardgames.library.domain.DomainToEntityMapper;
import com.boardgames.library.domain.EntityToDomainMapper;
import com.boardgames.library.domain.model.Game;
import com.boardgames.library.repository.GameRepository;
import com.boardgames.library.repository.jpa.JpaGameRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GameRepositoryImpl implements GameRepository {

    private final JpaGameRepository jpaGameRepository;

    public GameRepositoryImpl(final JpaGameRepository jpaGameRepository) {
        this.jpaGameRepository = jpaGameRepository;
    }

    @Override
    public List<Game> findAll() {
        return EntityToDomainMapper.convert(jpaGameRepository.findAll());
    }

    @Override
    public Game create(Game game) {
        return EntityToDomainMapper.convert(jpaGameRepository.save(DomainToEntityMapper.convert(game)));
    }
}
