package com.boardgames.library.repository.jpa;

import com.boardgames.library.domain.entity.GameEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaGameRepository extends JpaRepository<GameEntity, Integer> {
}
