package com.boardgames.library.repository;

import com.boardgames.library.domain.model.Game;

import java.util.List;

public interface GameRepository {

    List<Game> findAll();

    Game create(Game game);
}
