package com.boardgames.library.domain;

import com.boardgames.library.domain.model.Game;
import com.boardgames.library.domain.web.GameForm;

public class WebToDomainMapper {

    public static Game convert(com.boardgames.library.domain.web.Game from) {
        return Game.newBuilder()
                .id(from.getId())
                .name(from.getName())
                .build();
    }

    public static Game convert(GameForm from) {
        return Game.newBuilder()
                .name(from.getName())
                .build();
    }
}
