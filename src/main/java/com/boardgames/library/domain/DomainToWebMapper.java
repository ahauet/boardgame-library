package com.boardgames.library.domain;


import com.boardgames.library.domain.web.Game;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DomainToWebMapper {

    public static Game convert(com.boardgames.library.domain.model.Game from) {
        Game game = new Game();
        game.setId(from.getId());
        game.setName(from.getName());
        return game;
    }

    public static List<Game> convert(List<com.boardgames.library.domain.model.Game> from) {
        return from == null
                ? Collections.emptyList()
                : from.stream()
                .map(DomainToWebMapper::convert)
                .collect(Collectors.toList());
    }
}
