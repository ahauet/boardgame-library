package com.boardgames.library.domain.model;

public class Game {

    private final Long id;
    private final String name;

    private Game(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {

        private Long id;
        private String name;

        private Builder() {}

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Game build() {
            return new Game(this);
        }
    }
}
