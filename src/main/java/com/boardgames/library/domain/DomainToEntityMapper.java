package com.boardgames.library.domain;

import com.boardgames.library.domain.entity.GameEntity;
import com.boardgames.library.domain.model.Game;

public class DomainToEntityMapper {

    public static GameEntity convert(Game from) {
        GameEntity entity = new GameEntity();
        entity.setName(from.getName());
        return entity;
    }
}
