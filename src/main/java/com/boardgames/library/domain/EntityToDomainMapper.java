package com.boardgames.library.domain;

import com.boardgames.library.domain.entity.GameEntity;
import com.boardgames.library.domain.model.Game;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class EntityToDomainMapper {

    public static List<Game> convert (final List<GameEntity> from) {
        return from == null
                ? Collections.emptyList()
                : from.stream()
                .map(EntityToDomainMapper::convert)
                .collect(Collectors.toList());
    }

    public static Game convert (final GameEntity from) {
        return Game.newBuilder()
                .id(from.getId())
                .name(from.getName())
                .build();
    }
}
