package com.boardgames.library;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoardgamesLibraryApplication {

    public static void main(String[] args) {
        SpringApplication.run(BoardgamesLibraryApplication.class, args);
    }

}
