package com.boardgames.library.controller;

import com.boardgames.library.domain.DomainToWebMapper;
import com.boardgames.library.domain.WebToDomainMapper;
import com.boardgames.library.domain.web.Game;
import com.boardgames.library.domain.web.GameForm;
import com.boardgames.library.services.GameService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class GamesController {

    private final GameService gameService;

    public GamesController(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping("/games")
    public List<Game> getGames() {
        return DomainToWebMapper.convert(gameService.getGames());
    }

    @PostMapping("/games")
    public Game postGames(@RequestBody GameForm form) {
        return DomainToWebMapper.convert(gameService.createGame(WebToDomainMapper.convert(form)));
    }
}
