package com.boardgames.library.services;

import com.boardgames.library.domain.model.Game;
import com.boardgames.library.repository.GameRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GameService {

    private final GameRepository gameRepository;

    public GameService(final GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public List<Game> getGames() {
        return gameRepository.findAll();
    }

    public Game createGame(Game game) {
        return gameRepository.create(game);
    }
}